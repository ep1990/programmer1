-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 11, 2020 at 01:59 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cart_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `full_name` varchar(255) DEFAULT '',
  `email` varchar(255) DEFAULT '',
  `password` varchar(100) DEFAULT '',
  `profile_pic` varchar(255) DEFAULT '',
  `remember_token` varchar(100) DEFAULT '',
  `is_active` tinyint(1) DEFAULT '1',
  `credit_limit` double(20,2) DEFAULT '0.00',
  `created_at` int(11) DEFAULT '0',
  `updated_at` int(11) DEFAULT NULL,
  `deleted_at` int(11) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT '',
  `total_credit_limit` double(20,2) NOT NULL DEFAULT '0.00',
  `address` text,
  `longitude` double DEFAULT '0',
  `latitude` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `role_id`, `full_name`, `email`, `password`, `profile_pic`, `remember_token`, `is_active`, `credit_limit`, `created_at`, `updated_at`, `deleted_at`, `user_name`, `total_credit_limit`, `address`, `longitude`, `latitude`) VALUES
(1, 1, 'Admin', 'admin@admin.com', '$2y$10$6Zp8vAc0YAM3n0q8EzZraehgwMhlThaUzEgM832a2ME.em5aab.Ky', 'uploads/admin/profile1-1585930578.png', 'TzVbTuuWHegTjuREZu7DxKJuVIpAcUwgMxSYevbLvlTowQJIW8cQs6pHE0z5', 1, NULL, 1545489591, 1585930578, NULL, 'Admin', 0.00, 'Dubai - United Arab Emirates', 55.270782800000006, 25.2048493),
(2, 3, 'aftab', 'demo@admin.com', '$2y$10$VcH6vPszagoaM8MXi/qkqegy8gdxJss012DDOlimotRXBbSuporRu', '', '', 1, 0.00, 1578559922, 1578564672, 1578564672, 'Admin21', 0.00, NULL, 0, 0),
(3, 7, 'aftab', 'testing@gmail.com', '$2y$10$QtRIgYTA7JUkP2EacYfR7O15uJ466rXYVyykrzUBxtkHi3Vqa2aiG', 'uploads/media/about-02-1578564722.jpg', '', 1, 0.00, 1578564658, 1578564724, NULL, 'asasaads', 0.00, '', 0, 0),
(4, 2, 'imran', 'imran@gmail.com', '$2y$10$wSsS4f/LD9BnA1zdZyut4eOvpG.lg5PlCEoSsPtwDJ0lEwYjK/u8O', 'uploads/users/profile-1573134151-1580277100.jpg', '', 1, 0.00, 1580277100, 1580277100, NULL, 'imran', 0.00, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `product_price` int(20) NOT NULL,
  `quantity` int(20) NOT NULL,
  `total_price` int(20) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED DEFAULT '0',
  `title` longtext NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` int(20) NOT NULL,
  `updated_at` int(20) NOT NULL,
  `deleted_at` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `title`, `image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(151, 0, '{\r\n\"en\":\"Food\",\r\n\"ar\":\"\"\r\n}', 'uploads/categories/about-img-01-1580465145.png', 1571636554, 1582615912, 1582615933),
(154, 0, '{\"en\":\"Bar BQ\",\"ar\":\"\\u0634\\u0631\\u064a\\u0637 \\u0628 \\u0641\"}', 'uploads/categories/about-img-03-1580464565.png', 1580464565, 1582615933, 1582615933),
(155, 151, '{\"en\":\"Food sub category\",\"ar\":\"\"}', 'uploads/categories/location-1580466002.png', 1580466002, 1582615907, 1582615907),
(156, 155, '{\"en\":\"Food sub sub category\",\"ar\":\"\"}', 'uploads/categories/dentist-1580466284.png', 1580466284, 1580726748, NULL),
(157, 154, '{\"en\":\"sub cat bar bq\",\"ar\":\"\\u0634\\u0631\\u064a\\u0637 \\u0627\\u0644\\u0642\\u0637 \\u0627\\u0644\\u0641\\u0631\\u0639\\u064a \\u0628\\u0643\\u0631\\u064a\\u0644\"}', 'uploads/categories/about-img-02-1580467205.png', 1580467205, 1582615842, 1582615842),
(158, 157, '{\"en\":\"sub sub cat bar bq\",\"ar\":\"\"}', 'uploads/categories/about-img-01-1580467226.png', 1580467226, 1580467391, NULL),
(159, 154, '{\"en\":\"sub cat bar bqsdf\",\"ar\":\"\"}', '', 1581676357, 1582615901, 1582615901),
(160, 154, '{\"en\":\"sub category required\",\"ar\":\"\"}', '', 1582287314, 1582287320, NULL),
(162, 0, '{\"en\":\"perfumes\",\"ar\":\"\"}', 'uploads/categories/c2-1584424989.jpg', 1582615394, 1584424989, NULL),
(163, 0, '{\"en\":\"makeup\",\"ar\":\"\\u0645\\u064a\\u0643 \\u0623\\u0628\"}', 'uploads/categories/c1-1584424977.png', 1582615954, 1584424977, NULL),
(164, 0, '{\"en\":\"test f\",\"ar\":\"\"}', 'uploads/categories/about-tow-1583155280.jpg', 1583155280, 1583155294, 1583155294),
(165, 163, '{\"en\":\"testd\",\"ar\":\"\"}', '', 1583155301, 1583155317, 1583155317),
(166, 163, '{\"en\":\"testsd\",\"ar\":\"\"}', '', 1583155497, 1583155514, 1583155514),
(167, 0, '{\"en\":\"test\",\"ar\":\"\"}', 'uploads/categories/kisspng-2012-audi-r8-car-audi-5ab5179889d049-1583155554.jpg', 1583155554, 1583155608, 1583155608),
(168, 167, '{\"en\":\"dfsdfdf\",\"ar\":\"\"}', '', 1583155560, 1583155604, 1583155604),
(169, 162, '{\"en\":\"men\",\"ar\":\"\"}', 'uploads/categories/c2-1585550944.jpg', 1585550738, 1585550944, NULL),
(170, 162, '{\"en\":\"women\",\"ar\":\"\"}', 'uploads/categories/c2-1585550958.jpg', 1585550755, 1585550958, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `conversion_rates`
--

CREATE TABLE `conversion_rates` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `currency_from` bigint(20) UNSIGNED NOT NULL,
  `currency_to` bigint(20) UNSIGNED NOT NULL,
  `rate` double(10,2) NOT NULL,
  `refresh_at` int(11) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `conversion_rates`
--

INSERT INTO `conversion_rates` (`id`, `currency_from`, `currency_to`, `rate`, `refresh_at`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 1, 2, 0.27, 1586613560, 1521208667, 1586613500, NULL),
(4, 2, 1, 3.67, 1586613560, 1521208667, 1586613500, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` longtext NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `title`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'AED', 1, 0, 0, NULL),
(2, 'USD', 1, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `short_code` varchar(255) DEFAULT '',
  `is_active` tinyint(1) DEFAULT '1',
  `title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` int(11) NOT NULL DEFAULT '0',
  `updated_at` int(11) NOT NULL DEFAULT '0',
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `short_code`, `is_active`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'ar', 1, 'العربي', 0, 0, NULL),
(2, 'en', 1, 'English', 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('imran@gmail.com', '$2y$10$NHWuaVdBRZPhLovy1mZFLee4zqLAiMRZ9TCNANflC5BAtPRkZzVnu', '2020-04-03 15:31:52');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `slug` varchar(255) NOT NULL,
  `title` longtext NOT NULL,
  `price` bigint(20) NOT NULL DEFAULT '0',
  `quantity` int(20) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_at` int(11) NOT NULL DEFAULT '0',
  `updated_at` int(11) NOT NULL DEFAULT '0',
  `deleted_at` int(11) DEFAULT NULL,
  `description` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `slug`, `title`, `price`, `quantity`, `image`, `created_at`, `updated_at`, `deleted_at`, `description`) VALUES
(1097, 'first-product', 'Our Product', 100, 2, 'assets/front/images/product.jpg', 1580735433, 1582200734, 1582200734, 'Short description about product'),
(1098, 'second-product', 'Our Product', 20, 12, 'assets/front/images/product.jpg', 1580738038, 1585749168, NULL, 'Short description about product'),
(1110, 'my-product-with-lat-long', 'Our Product', 200, 20, 'assets/front/images/product.jpg', 1581316906, 1581680889, NULL, 'Short description about product'),
(1111, 'my-product-update', 'Our Product', 200, 17, 'assets/front/images/product.jpg', 1581928375, 1581936864, NULL, 'Short description about product'),
(1112, 'my-product-update-2', 'Our Product', 200, 17, 'assets/front/images/product.jpg', 1581934202, 1581935682, NULL, 'Short description about product'),
(1113, 'my-product-update-3', 'Our Product', 200, 20, 'assets/front/images/product.jpg', 1581934239, 1581934239, NULL, 'Short description about product'),
(1120, 'designing-phase-1', 'Our Product', 55, 55, 'assets/front/images/product.jpg', 1582011067, 1582200723, 1582200723, 'Short description about product'),
(1121, 'test', 'Our Product', 200, 20, 'assets/front/images/product.jpg', 1582203317, 1582203317, NULL, 'Short description about product'),
(1122, 'test-2', 'Our Product', 200, 20, 'assets/front/images/product.jpg', 1582203354, 1585747542, NULL, 'Short description about product'),
(1123, 'designing-phase-1', 'Our Product', 200, 20, 'assets/front/images/product.jpg', 1582203407, 1582203407, NULL, 'Short description about product'),
(1124, 'designing-phase-1-2', 'Our Product', 200, 20, 'assets/front/images/product.jpg', 1582204048, 1582204048, NULL, 'Short description about product'),
(1125, 'test-3', 'Our Product', 200, 20, 'assets/front/images/product.jpg', 1582205181, 1582205279, 1582205279, 'Short description about product'),
(1126, 'test-3', 'Our Product', 200, 20, 'assets/front/images/product.jpg', 1582205271, 1582205483, NULL, 'Short description about product'),
(1127, 'test-4', 'Our Product', 200, 10, 'assets/front/images/product.jpg', 1582206610, 1582206610, NULL, 'Short description about product'),
(1128, 'designing-phase-1-3', 'Our Product', 200, 10, 'assets/front/images/product.jpg', 1582207042, 1582207042, NULL, 'Short description about product'),
(1129, 'test-5', 'Our Product', 200, 20, 'assets/front/images/product.jpg', 1582207121, 1586607242, NULL, 'Short description about product'),
(1130, 'test-6', 'Our Product', 200, 20, 'assets/front/images/product.jpg', 1582207384, 1582207384, NULL, 'Short description about product'),
(1131, 'test-7', 'Our Product', 200, 20, 'assets/front/images/product.jpg', 1582207500, 1582207500, NULL, 'Short description about product'),
(1132, 'test-8', 'Our Product', 200, 30, 'assets/front/images/product.jpg', 1582266499, 1582266499, NULL, 'Short description about product'),
(1133, 'test-9', 'Our Product', 200, 20, 'assets/front/images/product.jpg', 1582267781, 1582541007, NULL, 'Short description about product'),
(1134, 'designing-phase-1-4', 'Our Product', 20, 0, 'assets/front/images/product.jpg', 1582268134, 1586182857, NULL, 'Short description about product'),
(1135, 'test-10', 'Our Product', 200, 20, 'assets/front/images/product.jpg', 1582284882, 1582287200, 1582287200, 'Short description about product'),
(1136, 'test-10', 'Our Product', 200, 18, 'assets/front/images/product.jpg', 1582549522, 1586608781, NULL, 'Short description about product'),
(1137, 'test-11', 'Our Product', 200, 18, 'assets/front/images/product.jpg', 1582552283, 1586602526, NULL, 'Short description about product'),
(1138, 'perfume-updated', 'Our Product', 200, 20, 'assets/front/images/product.jpg', 1582627693, 1586608785, NULL, 'Short description about product'),
(1139, 'perfume', 'Our Product', 200, 12, 'assets/front/images/product.jpg', 1582721422, 1586608676, NULL, 'Short description about product'),
(1140, 'perfume-2', 'Our Product', 200, 11, 'assets/front/images/product.jpg', 1582721560, 1586613399, NULL, 'Short description about product'),
(1141, 'hair-spray', 'Our Product', 200, 478, 'assets/front/images/product.jpg', 1582722465, 1586613397, NULL, 'Short description about product'),
(1142, 'our-product', 'Our Product', 200, 187, 'uploads/products/1-1586611086.jpg', 1583490047, 1586611443, 1586611443, 'Short description about product'),
(1143, 'testing', 'Our Product', 20, 20, 'assets/front/images/product.jpg', 1585289059, 1585289141, 1585289141, 'Short description about product'),
(1144, 'my-new-product', 'my new product', 100, 20, 'uploads/products/product-1586613383.jpg', 1586611496, 1586613400, NULL, 'this is description');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(255) DEFAULT '',
  `last_name` varchar(255) DEFAULT '',
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `deleted_at` int(11) DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `created_at`, `updated_at`, `deleted_at`, `remember_token`) VALUES
(1, '', '', NULL, '$2y$10$RADRE4xlEB5ILu/rb1MFV.MRRY79dlGkBqCpxAy1VTFbiyZumloW6', 1582898342, 1582898543, NULL, NULL),
(4, '', '', NULL, '$2y$10$DWlSTHahnMh5mULYiStSCuTzcgOMXduoroeINUpdLHL1IsL98DV3O', 1583261348, 1583261348, NULL, NULL),
(5, '', '', NULL, '$2y$10$0J.1NU5hfe5bjjzK9Xq6QO4RsuQ2R1p.BD03NrLB2exGtxfeETpxK', 1583261407, 1583261407, NULL, NULL),
(8, '', '', NULL, '$2y$10$FYfzb9mv7gAZonhTRw5z7.ibPjW0l5cnDWnSju/ncryttdd3pA6Ui', 1584643017, 1584643318, NULL, NULL),
(9, '', '', NULL, '$2y$10$K0G5MYja9zKuwBejFsCZ0uv3X96B/DtiGJUa9Yfo7CpwGOqnyjYH2', 1585492357, 1585492395, NULL, NULL),
(10, 'Muhammad Imran', 'Israr', 'imran@gmail.com', '$2y$10$iAv86s2DW8tZnxxNgK4sP.aouYYJD1NzsFuaGt0bUGYwxdlOzUFLi', 1585733558, 1585755626, NULL, 'KpreDRt1PeLycNC4sU8NXB8sRQ0JDyZAnWIFg3vY1CWJk0watAJ9gNzLOK3q'),
(11, 'Muhammad Imran', 'Israr', 'imranisrar@gmail.com', '$2y$10$hAf/a/KA5xRG4uizwRWkpeLH/tppQ4jCo2WoaMWexrApJ7Ce6ZRWK', 1585929010, 1585929041, NULL, NULL),
(12, 'first', 'last', 'firslast@gmail.com', '$2y$10$SOf/Q/gjGc1BvRPCpHMuNusJEGPZEWhTW5RNai5i9q90z7U4Tslnm', 1586604607, 1586604607, NULL, NULL),
(13, 'nouman', 'israr', 'nouman2@gmail.com', '$2y$10$UMaRtsEK/mKj96qBprvMBewpMoIoa41cUAoFOBTAcsEjGVL2yh/rG', 1586605971, 1586605971, NULL, 'caMpDapq0wubQyoNxyirLlnapQshqqjSR2wJN2fnZ3KEeSr7QL4buIZR9GY2'),
(14, 'muhammad', 'nouman', 'muhammad1@gmail.com', '$2y$10$ZPPX6CFFCzWRRaM4ZAzFv.D4lzM2IwecqGFsumlWaed4bmqBgsyd2', 1586608131, 1586608131, NULL, NULL),
(15, 'muhammad', 'farhan', 'farhan1@gmail.com', '$2y$10$G.9bjfgTwdulDPVZTr6mSuJb3SXzChYsc05FUhtizc9wmTGO629JS', 1586612212, 1586612212, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foriegn_key` (`role_id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_carts_products1_idx` (`product_id`),
  ADD KEY `fk_carts_users1_idx` (`user_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexes for table `conversion_rates`
--
ALTER TABLE `conversion_rates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_conversion_rates_currencies1_idx` (`currency_from`),
  ADD KEY `fk_conversion_rates_currencies2_idx` (`currency_to`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- AUTO_INCREMENT for table `conversion_rates`
--
ALTER TABLE `conversion_rates`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1145;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admins`
--
ALTER TABLE `admins`
  ADD CONSTRAINT `foriegn_key` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
