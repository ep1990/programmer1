<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// use App\Events\newNotifications;
// route::get('clear-all' , function (){
//     \Artisan::call('clear:all');
//     \Artisan::call('queue:work --once');
// });


Route::group([ 'prefix' => 'admin','namespace' => 'Admin','as' => 'admin.'], function () {
    Route::group(['as' => 'auth.'], function () {
        Route::get('/', ['uses' => 'Auth\LoginController@showLoginForm', 'as' => 'login.show-login-form']);
        Route::post('login', ['uses' => 'Auth\LoginController@login', 'as' => 'login.login']);
        Route::post('logout', ['uses' => 'Auth\LoginController@logout', 'as' => 'login.logout']);
    });

    Route::group(['middleware' => 'auth:admin'], function () {
        Route::get('home', ['uses' => 'HomeController@index', 'as' => 'home.index']);
        Route::get('add-product/{id}', ['uses' => 'ProductsController@index', 'as' => 'product']);
        Route::post('product-action/{id}', ['uses' => 'ProductsController@action', 'as' => 'product-action']);
        Route::get('delete-product/{id}', ['uses' => 'ProductsController@destroy', 'as' => 'delete-product']);
        Route::post('upload-image', ['uses' => 'HomeController@uploadImage', 'as' => 'upload-image']);
        Route::post('delete-image', ['uses' => 'HomeController@DeleteImage', 'as' => 'delete-image']);
    });

});

route::get('/',['uses' => 'Admin\Auth\LoginController@showLoginForm', 'as' => 'login.show-login-form' ]);

Route::get('/login',function (){

})->name('auth.login.show-login-form');

Route::group([ 'namespace' => 'Front','as' => 'front.'],function(){
    Route::get('/404',['uses' => 'IndexController@error404','as' => '404' ]);
    Route::get('/',['uses' => 'IndexController@index','as' => 'index' ]);
    route::get('/product/{slug}/detail',['uses' => 'IndexController@detail','as' => 'product.detail']);


    Route::group(['as' => 'auth.'],function (){
    Route::get('/login',['uses' => 'Auth\LoginController@showLoginForm','as' => 'login']);
    Route::post('/login',['uses' => 'Auth\LoginController@login','as' => 'login.submit']);
    Route::post('/register',['uses' => 'Auth\RegisterController@register','as' => 'register']);
    Route::get('/register',['uses' => 'Auth\RegisterController@showRegistrationForm','as' => 'register-page']);
    Route::get('/logout',['uses' => 'Auth\LoginController@logout','as' => 'logout']);
    

   });

  route::group(['middleware' => ['auth', 'cart']],function (){
      /* Add to cart routes*/
      route::post('add-to-cart',['uses' => 'CartController@add','as' => 'cart.add']);
      route::get('cart',['uses' => 'CartController@index','as' => 'cart.index']);
      route::get('get-cart-data',['uses' => 'CartController@getCart','as' => 'get-cart-data']);
      route::post('update-cart',['uses' => 'CartController@updateCart','as' => 'cart.update']);
      route::get('delete-cart/{id}',['uses' => 'CartController@deleteCart','as' => 'cart.delete']);

  });
});

