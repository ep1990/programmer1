<?php

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

use App\Events\newNotifications;

Route::group(['namespace' => 'Admin', 'as' => 'api.', 'prefix' => 'api'], function() {

    Route::get('sub-categories/{id}', ['uses' => 'ProductsController@getSubCategories1', 'as' => 'products.get.subCategories']);
    Route::post('sub-attributes', ['uses' => 'ProductsController@getSubAttributesAjax', 'as' => 'products.get.subAttributes']);

});

Route::group(['namespace' => 'Front','as' => 'api.','prefix' => 'api'],function(){
    Route::get('category/sub-categories/{id}',['uses' => 'CategoryController@getSubCategories1','as'=>'categories.get.subCategories']);

});
Route::group(['namespace' => 'Api' , 'as' => 'api.', 'prefix' => 'api'] ,function (){
    Route::get('cities', ['uses' => 'IndexController@getCities', 'as' => 'Country.get.cities']);
    Route::post('user/register', ['uses' => 'UsersController@register', 'as' => 'users.register']);
    Route::post('supplier/register', ['uses' => 'UsersController@supplierRegister', 'as' => 'supplier.register']);
    Route::post('user/login', ['uses' => 'UsersController@login', 'as' => 'users.login']);
    Route::get('user/social-login', ['uses' => 'UsersController@socialLoginCheck', 'as' => 'social-login']);
    Route::post('user/social/login-check', ['uses' => 'UsersController@socialLoginCheck', 'as' => 'social-login-check']);
    Route::get('user/social/user-information', ['uses' => 'UsersController@getSocialUserInformation', 'as' => 'social-user-information']);

    /*
        * WITHDRAWS CONTROLLER
        */
    Route::get('withdraws/show', ['uses' => 'WithdrawsController@show', 'as' => 'withdraws.show']);
    Route::post('withdraws/save-request', ['uses' => 'WithdrawsController@saveRequest', 'as' => 'withdraws.save-request']);


    Route::post('user/make-product-favourite', ['uses' => 'UsersController@makeProductFavorite', 'as' => 'users.make-product-favourite']);
    Route::post('user/make-product-un-favourite', ['uses' => 'UsersController@makeProductUnFavorite', 'as' => 'users.make-product-un-favourite']);
    Route::get('user/favourite-cars', ['uses' => 'UsersController@favoritesCars', 'as' => 'users.favourite-cars']);
    Route::post('upload-image', ['uses' => 'IndexController@uploadImage', 'as' => 'upload-image']);

    Route::group(['middleware'=> 'jwt.verify'],function (){
        Route::post('user/verify-email', ['uses' => 'UsersController@verifyEmail', 'as' => 'users.verify-email']);
        Route::get('user/resend-verification-code', ['uses' => 'UsersController@resendVerificationCode', 'as' => 'users.resend-verification-code']);
        Route::get('user/profile-data', ['uses' => 'UsersController@userProfile', 'as' => 'users.profile-data']);
        Route::post('user/profile', ['uses' => 'UsersController@profile', 'as' => 'users.profile']);
        Route::post('user/change-password', ['uses' => 'UsersController@changePassword', 'as' => 'users.change-password']);
        Route::post('user/forgot-password', ['uses' => 'UsersController@forgotPassword', 'as' => 'users.forgot-password']);
        Route::post('user/reset-password', ['uses' => 'UsersController@resetPassword', 'as' => 'users.reset-password']);
        Route::get('user/logout', ['uses' => 'UsersController@logout', 'as' => 'users.logout']);
        Route::get('user/logout', ['uses' => 'UsersController@logout', 'as' => 'users.logout']);
        Route::get('user/edit/payment-profile', ['uses' => 'UsersController@paymentProfile', 'as' => 'users.edit-payment-ptofile']);
        Route::post('user/update/payment-profile', ['uses' => 'UsersController@savePaymentProfile', 'as' => 'users.save-payment-ptofile']);
        Route::get('supplier/profile-data', ['uses' => 'UsersController@supplierProfile', 'as' => 'supplier.profile-data']);
        Route::post('supplier/profile', ['uses' => 'UsersController@supplierkProfileUpdate', 'as' => 'supplier.profile']);
    });
    Route::post('suppliers/search', ['uses' => 'SuppliersController@search', 'as' => 'suppliers.search']);
    Route::post('supplier/detail', ['uses' => 'SuppliersController@detail', 'as' => 'supplier.detail']);
    Route::post('supplier/products', ['uses' => 'SuppliersController@products', 'as' => 'supplier.products']);
    Route::post('review-supplier', ['uses' => 'SuppliersController@reviewSupplier', 'as' => 'supplier.products']);


    /* Index controller*/
    Route::get('settings', ['uses' => 'IndexController@settings', 'as' => 'settings']);
    Route::post('pages', ['uses' => 'IndexController@pages', 'as' => 'pages']);
    Route::get('stores', ['uses' => 'IndexController@getAllStores', 'as' => 'store.products']);
    Route::get('store-details', ['uses' => 'IndexController@storeDetails', 'as' => 'store.details']);
    Route::post('contact', ['uses' => 'IndexController@contactUs', 'as' => 'contact']);

    /* Agencies Controller*/
    Route::get('agencies', ['uses' => 'AgenciesController@agencies', 'as' => 'agencies']);
    Route::post('agency-details', ['uses' => 'AgenciesController@agencyDetails', 'as' => 'agencies.details']);
    Route::post('agency-branches', ['uses' => 'AgenciesController@agencyBranches', 'as' => 'agencies.branches']);
    Route::post('agency-vehicles', ['uses' => 'AgenciesController@agencyVehicles', 'as' => 'agencies.vehicles']);
    Route::post('search-agency', ['uses' => 'AgenciesController@searchAgency', 'as' => 'agencies.search']);


    /*Categories Controller */
    Route::get('categories', ['uses' => 'ProductsController@categories', 'as' => 'categories']);
    Route::get('sub-categories', ['uses' => 'CategoriesController@getSubcategories', 'as' => 'categories.sub-categories']);
    Route::post('brand-models', ['uses' => 'CategoriesController@getBrandsModels', 'as' => 'brands.models']);
    Route::post('search-brand', ['uses' => 'CategoriesController@brandSearch', 'as' => 'brands.search']);


    /*Car Controller*/
    Route::post('car-details', ['uses' => 'CarsController@details', 'as' => 'cars.detail']);
    Route::post('car/book-test-drive', ['uses' => 'CarsController@bookTestDrive', 'as' => 'cars.book-test-drive']);
    Route::post('cars', ['uses' => 'CarsController@searchCars', 'as' => 'cars.search']);

    /* PRODUCTS */
    Route::get('products', ['uses' => 'ProductsController@index', 'as' => 'products']);
    Route::post('products/search', ['uses' => 'ProductsController@search', 'as' => 'products.search']);
    Route::post('product/detail', ['uses' => 'ProductsController@detail', 'as' => 'product.detail']);
    Route::post('product/add', ['uses' => 'ProductsController@add', 'as' => 'product.add']);
    Route::post('product-image-upload', ['uses' => 'ProductsController@uploadImage', 'as' => 'product-image-upload']);
    Route::delete('product-delete/{id}', ['uses' => 'ProductsController@destroy', 'as' => 'product.destroy']);
    Route::post('review-product', ['uses' => 'ProductsController@reviewProduct', 'as' => 'product.review']);

    /* REVIEWS */
    Route::post('reviews', ['uses' => 'ProductsController@getReviews', 'as' => 'kiosk.reviews']);

    /* Order Controller*/
    Route::post('update-address', ['uses' => 'OrdersController@updateAddress', 'as' => 'checkout.update-address']);
    Route::get('order', ['uses' => 'OrdersController@index', 'as' => 'order.index']);
    Route::post('create-order', ['uses' => 'OrdersController@createOrder', 'as' => 'order.create']);
    Route::post('review-order', ['uses' => 'OrdersController@reviewOrder', 'as' => 'order.review']);
    Route::post('user/orders', ['uses' => 'OrdersController@getUserOrders', 'as' => 'user.orders.listing']);
    Route::post('supplier/orders', ['uses' => 'OrdersController@getSupplierOrders', 'as' => 'supplier.orders.listing']);
    Route::post('supplier/order-detail', ['uses' => 'OrdersController@supplierOrderDetail', 'as' => 'supplier.order.detail']);
    Route::post('supplier/cancel-order', ['uses' => 'OrdersController@supplierCancelOrder', 'as' => 'supplier.cancel.order']);
    Route::post('supplier/confirm-order', ['uses' => 'OrdersController@supplierConfirmOrder', 'as' => 'supplier.confirm.order']);
    Route::post('supplier/shipped-order', ['uses' => 'OrdersController@supplierShippedOrder', 'as' => 'supplier.shipped.order']);
    Route::post('supplier/deliver-order', ['uses' => 'OrdersController@supplierDeliverOrder', 'as' => 'supplier.shipped.order']);
    Route::post('user/order-detail', ['uses' => 'OrdersController@userOrderDetail', 'as' => 'user.order.detail']);
    // Route::post('coupon/apply', ['uses' => 'OrdersController@couponApply', 'as' => 'coupon-apply']);
    // Route::post('coupon/remove', ['uses' => 'OrdersController@couponRemove', 'as' => 'coupon-remove']);

    

    /*Cart Controller*/
    Route::post('cart/add', ['uses' => 'CartController@addCart', 'as' => 'cart.add']);
    Route::post('cart/update', ['uses' => 'CartController@updateCart', 'as' => 'cart.update']);
    Route::delete('cart/delete/{id}', ['uses' => 'CartController@deleteCart', 'as' => 'cart.delete']);
    Route::get('cart/view', ['uses' => 'CartController@index', 'as' => 'cart.index']);
    route::get('valid-coupon/{code}',['uses' => 'CartController@validCoupon','as' => 'cart.valid.coupon']);
    


    /*CheckOut Controller*/
    // Route::get('checkout', ['uses' => 'CheckOutController@index', 'as' => 'checkout.index']);
    // Route::post('update-address', ['uses' => 'CheckOutController@updateAddress', 'as' => 'checkout.update-address']);

    /* Order Controller*/
    // Route::get('order', ['uses' => 'OrdersController@index', 'as' => 'order.index']);
    // Route::post('create-order', ['uses' => 'OrdersController@createOrder', 'as' => 'order.create']);
    // Route::post('review-order', ['uses' => 'OrdersController@reviewOrder', 'as' => 'order.review']);

    /*User Orders*/
    // Route::get('orders/{status}', ['uses' => 'OrdersController@getUserOrders', 'as' => 'orders.listing']);
    // Route::get('user/order-detail', ['uses' => 'OrdersController@userOrderDetail', 'as' => 'user.order.detail']);



    /* Chef Orders*/
    // Route::get('store-orders/{status}', ['uses' => 'OrdersController@getChefOrders', 'as' => 'chef.orders.listing']);
    // Route::get('store/order-detail', ['uses' => 'OrdersController@storeOrderDetail', 'as' => 'store.order.detail']);
    // Route::post('store/cancel/rider-order', ['uses' => 'OrdersController@storeCancelRiderOrder', 'as' => 'store.cancel.rider-order']);
    // Route::post('store/cancel-order', ['uses' => 'OrdersController@storeCancelOrder', 'as' => 'store.cancel.order']);


    /*Riders Controller*/
    // Route::get('riders', ['uses' => 'RidersController@getRiders', 'as' => 'riders.all']);
    // Route::get('rider-orders', ['uses' => 'RidersController@getRiderOrders', 'as' => 'rider.order-list']);
    // Route::post('deliver-order', ['uses' => 'RidersController@deliverOrder', 'as' => 'riders.deliver-order']);
    // Route::post('rider/accept-order', ['uses' => 'RidersController@acceptOrder', 'as' => 'riders.accept-order']);
    // Route::post('rider/order-details', ['uses' => 'RidersController@orderDetail', 'as' => 'riders.order-details']);
    // Route::post('rider/ignore-order', ['uses' => 'RidersController@ignoreOrder', 'as' => 'riders.ignore-order']);
    // Route::post('rider/order-delivered', ['uses' => 'RidersController@orderDelivered', 'as' => 'riders.order-delivered']);
    /*Notifications*/
    Route::group(['middleware' => 'jwt.verify'],function (){
        Route::get('notifications',['uses' =>'NotificationController@notifications','as' => 'notification.all' ]);
        Route::get('notifications-count',['uses' =>'NotificationController@notificationCount','as' => 'notification.count' ]);
        Route::get('notification-seen',['uses' =>'NotificationController@isSeen','as' => 'notification.seen' ]);
        Route::post('notification-read',['uses' =>'NotificationController@isRead','as' => 'notification.read' ]);
        Route::delete('notification-delete/{notificationId}',['uses' =>'NotificationController@deleteNotification','as' => 'notification.delete' ]);
        Route::delete('notifications-clear',['uses' =>'NotificationController@clearAll','as' => 'notification.clear.all' ]);
        Route::post('notification-status',['uses' =>'NotificationController@statusChange','as' => 'notification.status' ]);
    });

    /*Chat Controller*/
    Route::get('get-conversations', ['uses' => 'ChatController@getConversations', 'as' => 'chat.get-conversations']);
    Route::post('get-messages', ['uses' => 'ChatController@getMessages', 'as' => 'chat.get-messages']);
    Route::post('start-conversation', ['uses' => 'ChatController@startConversation', 'as' => 'chat.start-conversation']);
    Route::post('message-send', ['uses' => 'ChatController@messageSend', 'as' => 'chat.message-send']);
    Route::delete('conversation/delete/{id}', ['uses' => 'ChatController@conversationDelete', 'as' => 'chat.conversation-delete']);






});


// Route::get('check/auth',function(Request $request){
//     $temp = new stdClass();
//     $temp->id = 73;
//     broadcast(new newNotifications(new stdClass(), $temp));
// //    return redirect()->back();
// });