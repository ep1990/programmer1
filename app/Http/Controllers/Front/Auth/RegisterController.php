<?php

namespace App\Http\Controllers\Front\Auth;

use App\Http\Requests\User\UserRequest;
use App\Http\Services\users\UserService;
use App\Models\City;
use \App\Traits\Users;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Tymon\JWTAuth\Facades\JWTAuth;

class RegisterController extends Controller
{
    /*
      |--------------------------------------------------------------------------
      | Register Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users as well as their
      | validation and creation. By default this controller uses a trait to
      | provide this functionality without requiring any additional code.
      |
     */

    use RegistersUsers, Users;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/verification';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        parent::__construct();
    }

    public function showRegistrationForm()
    {
        return view('front.register');
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [

        ]);
    }
    public function register(UserRequest $request)
    {
        $userService = new UserService($request);
        $user = $userService->register();
        $this->guard()->login($user);
        $user['token'] = JWTAuth::fromUser(auth()->user());
        session()->put('USER_DATA', $user);
        session()->flash('status','Login Successful');
        return redirect(route('front.index'));
    }
}
