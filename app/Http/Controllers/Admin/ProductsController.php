<?php namespace App\Http\Controllers\Admin;

use App\Events\CartChangeNotifications;
use App\Events\newNotifications;
use App\Events\newPriceNotifications;
use App\Http\Services\ProductService;
use App\Models\Attribute;
use App\Models\Cart;
use App\Models\Category;
use App\Models\Notification;
use App\Models\Product;
use App\Http\Requests\ProductRequest;
use App\Http\Libraries\Uploader;
use App\Http\Libraries\DataTable;
use App\Http\Controllers\Controller;
use App\Models\ProductImage;
use App\Models\User;
use App\Traits\GetAttributes;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Symfony\Component\Console\Input\Input;
use App\Jobs\ProductDiscount;

class ProductsController extends Controller
{
   use GetAttributes;
    public function __construct()
    {
        parent::__construct('adminData', 'admin');
    }

    public function index($id)
    {
        $product = new Product;
        if($id > 0)
        {
            $check = $this->checkProductExist($id);
            if(!$check){
                return redirect(route('admin.home.index'))->with('error', 'Product not found');
            }
            $product = Product::where('id', $id)->with('images')->first();
            if (count($product->images) > 0){
                foreach ($product->images as $imagevalue){

                    $productImages[] = [$imagevalue->image,$imagevalue->type];
                }
                $product->images = $productImages;
            }
        }
         return view('admin.product-form', ['product' => $product]);
    }



    public function action(ProductRequest $request, $id){
        $message = "Product added successfully";
        if($id > 0){
            $check = $this->checkProductExist($id);
            if(!$check){
                return redirect(route('admin.home.index'))->with('error', 'Product not found');
            }
            ProductImage::where('product_id',$id)->delete();
            $message = "Product updated successfully";
        }
        $data = $request->only('title', 'description','price','quantity');
        $data['slug']= str_slug($request->title);
        $productExist =  Product::where('slug',$data['slug'])->first();
        if ($productExist !==  null){
            $data['slug'] = $this->incrementSlug($data['slug'],$id);
        }
        // if($request->has('image')){
        //     $uploader = new Uploader();
        //     $uploader->setInputName('image');
        //     if ($uploader->isValidFile()) {
        //         $uploader->upload('products', $uploader->fileName);
        //         if ($uploader->isUploaded()) {
        //             $data['image'] = $uploader->getUploadedPath();
        //         }
        //     }
        //     if (!$uploader->isUploaded()) {
        //         return redirect()->back()->withErrors('err', $uploader->getMessage())->withInput();
        //     }
        // }
        if ($request->has('images')){
            $images = json_decode($request->images);
            $data['image'] = $images[0][0];
        }
        $product = Product::updateOrCreate(['id' => $id], $data);
        if ($request->has('images')) {
            $imagesArray = json_decode($request->images);
            $uploadImages = [];
            foreach ($imagesArray as $key => $value){
                // $default = '0';
                // if ($request->default_image != '' && $request->default_image == $value[0]){
                //     $default = '1';
                // }
                $uploadImages[] = ['product_id' => $product->id, 'image' => $value[0],'default_image' => 0,'type' => 'interior'];

            }
            ProductImage::insert($uploadImages);
        }
        return redirect(route('admin.home.index'))->with('success', $message);
    }



    public function destroy($id)
    {
        $product=Product::where('id','=',$id)->firstOrFail();
        Product::destroy($id);
        return back()->with('success', 'Product deleted successfully');
    }




    public function incrementSlug($slug ,$id) {

        $original = $slug;

        $count = 2;
        if ($id == 0){
            while (Product::whereSlug($slug)->exists()) {

                $slug = "{$original}-" . $count++;
            }
        }
        else{
            while (Product::whereSlug($slug)->where('id','!=',$id)->exists()) {

                $slug = "{$original}-" . $count++;
            }
        }
        return $slug;

    }

    private function checkProductExist($id)
    {
        $product = Product::find($id); 
        if(!$product){
            return false;
        }
        return true;
    }

}
