<?php

namespace App\Http\Middleware;

use App\Jobs\GetVisitorInfo;
use Closure;

class GetVisitorStats
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->isMethod('get')) {
        $stats = [
            'city' => 'unknown',
            'country' => 'unknown',
            'region' => 'unknown',
            'postal_code' => '',
        ];
        $stats['user_ip'] = $request->ip();
        if (auth()->guest()) {
            $stats['user_url'] = 'Guest';
        }
        else {
            $stats['user_url'] = '<a href="'.route('admin.users.edit', ['users' => auth()->user()->id]).'">'.auth()->user()->first_name.'</a>';
        }
        $stats['browser'] = $request->header('User-Agent');
        $stats['browser_name'] = $this->getBrowser($stats['browser'])['name'];
        $stats['platform'] = $this->getBrowser($stats['browser'])['platform'];
        $stats['referer'] = $request->headers->get('referer');
        $stats['visit_route'] = $request->fullUrl();
        $json = file_get_contents("http://api.hostip.info/get_json.php?ip={$stats['user_ip']}&&position=true");
        $userDetail = json_decode($json, true);
        if (!empty($userDetail['city'])){
            $stats['city'] = $userDetail['city'];
        }
        if (!empty($userDetail['country_name'])){
            $stats['country'] = $userDetail['country_name'];
        }
        if (!empty($userDetail['region_name'])){
            $stats['region'] = $userDetail['region_name'];
        }
        if (!empty($userDetail['zip_code'])){
            $stats['postal_code'] = $userDetail['zip_code'];
        }
        if(!empty($userDetail['lat'])){
            $stats['latitude'] = $userDetail['lat'];
        }
        if(!empty($userDetail['lng'])){
            $stats['longitude'] = $userDetail['lng'];
        }
        $job = new GetVisitorInfo($stats);
            $job->delay(5);
            dispatch($job);
        }
        return $next($request);
    }

    function getBrowser($userAgent) {
        $u_agent = $userAgent;
        $bname = 'Unknown';
        $platform = 'Unknown';
        $ub="";
        $version= "";
        // First get the platform?
        if (preg_match('/linux/i', $u_agent)) {
            $platform = 'linux';
        } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'mac';
        } elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'windows';
        }
        // Next get the name of the useragent yes seperately and for good reason
        if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) {
            $bname = 'Internet Explorer';
            $ub = "MSIE";
        } elseif(preg_match('/Trident/i',$u_agent)) {
            $bname = 'Internet Explorer';
            $ub = "Trident";
        } elseif(preg_match('/Firefox/i',$u_agent)) {
            $bname = 'Mozilla Firefox';
            $ub = "Firefox";
        } elseif(preg_match('/Edge/i',$u_agent)) {
            $bname = 'Microsoft Edge';
            $ub = "Edge";
        } elseif(preg_match('/Chrome/i',$u_agent)) {
            $bname = 'Google Chrome';
            $ub = "Chrome";
        } elseif(preg_match('/Safari/i',$u_agent)) {
            $bname = 'Apple Safari';
            $ub = "Safari";
        } elseif(preg_match('/Opera/i',$u_agent)) {
            $bname = 'Opera';
            $ub = "Opera";
        } elseif(preg_match('/Netscape/i',$u_agent)) {
            $bname = 'Netscape';
            $ub = "Netscape";
        }
        // finally get the correct version number
        $known = array('Version', $ub, 'other');
        $pattern = '#(?<browser>' . join('|', $known) . ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $u_agent, $matches)) {
            // we have no matching number just continue
        }
        // see how many we have
        $i = count($matches['browser']);
        if ($i != 1) {
            //we will have two since we are not using 'other' argument yet
            //see if version is before or after the name
            if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
                $version= $matches['version'][0];
            } else {
                $version= $matches['version'][1];
            }
        } else {
            $version= $matches['version'][0];
        }
        // check if we have a number
        if ($version==null || $version=="") {$version="?";}
        return array(
            'userAgent' => $u_agent,
            'name'      => $bname,
            'version'   => $version,
            'platform'  => $platform,
            'pattern'    => $pattern
        );
    }


}
