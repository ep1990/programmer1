<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use App\Models\Currency;
use App\Models\ConversionRate;

class CurrencyConversion {
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {

//        \Cache::flush();
        $cache = cache();
        /*
         * PUT CURRENCIES IN CACHE
         */
        if (!$cache->has('CURRENCIES')) {
            $currencies = Currency::active()->get();
            $currenciesData = [];
            foreach ($currencies as $key => $currency) {
                $currenciesData[$currency->id] = $currency->toArray();
            }
            $cache->put('CURRENCIES_DATA', $currenciesData, Carbon::now()->addHour(env('HOURS_TO_EXPIRE_CURRENCIES_CACHE')));
            $cache->put('CURRENCIES', $currencies->pluck('title', 'id')->all(), Carbon::now()->addHour(env('HOURS_TO_EXPIRE_CURRENCIES_CACHE')));
        }
        /*
         * PUT CONVERSION RATES IN CACHE
         */
        if (!$cache->has('CONVERSION_RATES')) {
            $conversionData = [];
            $conversionRates = ConversionRate::where('refresh_at', '>', Carbon::now()->timestamp)->get();
            $expiresAt = Carbon::now()->addMinutes(env('MINUTES_TO_EXPIRE_CONVERSION_RATES_CACHE'));
            if (count($conversionRates) <= 0) {
                // insert conversion rates
                $timestamp = $expiresAt->timestamp; // refresh rates after ? minutes
                foreach (config('app.conversion_rates') as $key => $fromTo) {

                    $currencyIDsFromTo = explode('_to_', $key); // exploding e.g. 1_to_2 to get 1 and 2 which are currency IDs
//                    if($fromTo = 'AED/USD'){
//                        $rate = getAEDCoversationRate();
//                    }else{
//                        $rate = getUSDCoversationRate();
//                    }
                    $conversionData[$key] = ConversionRate::updateOrCreate(['currency_from' => $currencyIDsFromTo[0], 'currency_to' => $currencyIDsFromTo[1]], [
                        'currency_from' => $currencyIDsFromTo[0],
                        'currency_to' => $currencyIDsFromTo[1],
                        'rate' => ($fromTo == 'AED/USD') ? 0.27 : 3.67,
                        'refresh_at' => $timestamp,
                    ])->toArray();
                }
                $conversionData['refresh_at'] = $timestamp;
            }
            else {
                foreach ($conversionRates as $key => $currencyRate) {
                    $conversionData[$currencyRate->currency_from.'_to_'.$currencyRate->currency_to] = $currencyRate->toArray();
                    $conversionData['refresh_at'] = $currencyRate->refresh_at;
                }
            }
            $cache->put('CONVERSION_RATES', $conversionData, $expiresAt);
        }
        /*
         * PUT SITE SETTINGS IN CACHE
         */
//        if (!$cache->has('SITE_SETTINGS')) {
//            $siteSettings = SiteSetting::findOrFail(SiteSetting::$SITE_SETTINGS_ID);
//            unset($siteSettings->created_at);
//            unset($siteSettings->updated_at);
//            unset($siteSettings->deleted_at);
//            $cache->put('SITE_SETTINGS', $siteSettings->toArray(), Carbon::now()->addMinutes(env('MINUTES_TO_EXPIRE_SITE_SETTINGS_CACHE')));
//        }
        /*
         * SET TIMEZONE
         */
//        $siteSettings = $cache->get('SITE_SETTINGS');
//        app('config')->set('app.timezone', $siteSettings['time_zone']);
        return $next($request);
    }

}
