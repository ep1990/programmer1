<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\Console\Input\Input;

class FromValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $routeAlias = $this->route()->action['as'];
        $rules = [];
        if (str_contains($routeAlias, 'admin.category.save')) {
            $rules = [
                'title' => 'required|max:255',
                'image' => 'image',
            ];
        }
        if (str_contains($routeAlias, 'admin.event.store')) {
           Carbon::parse($this->start_date);
           Carbon::parse($this->end_date);
            $rules = [
                'title' => 'required|max:75',
                'description' => 'required',
                'start_date' => 'required|date|max:255',
                'end_date' => 'required|max:255|after_or_equal:start_date',
                'event_location' => 'required|max:255',
            ];
        }
        if (str_contains($routeAlias, 'admin.slider.store')) {
            $rules = [
                'images' => 'mimes:jpeg,jpg,png,gif|max:10000'
            ];
        }
        if (str_contains($routeAlias, 'admin.products.product-images.store')) {
            $rules = [
                'images' => 'mimes:jpeg,jpg,png,gif|max:10000'
            ];
        }if (str_contains($routeAlias, 'admin.attributes.update')) {
            $rules = [
                'name' => 'required|max:255'
            ];
        }
        if (str_contains($routeAlias, 'admin.attributes.sub-attributes.update')) {
            $rules = [
                'name' => 'required|max:255'
            ];
        }
        if (str_contains($routeAlias, 'admin.categories.sub-categories.update')) {
            $rules = [
                'title' => 'required|max:255',
            ];
        }
        if (str_contains($routeAlias, 'admin.countries.update')) {
            $rules = [
                'name' => 'required|max:255'
            ];
        }
        if (str_contains($routeAlias, 'admin.countries.cities.update')) {
            $rules = [
                'name' => 'required|max:255'
            ];
        }
        if (str_contains($routeAlias, 'admin.agencies.update')) {
            $rules = [
                'name' => 'required|max:255',
                'country_id' => 'required',
                'city_id' => 'required',
                'email'=>'required',
                'landline' => ['required','regex:/^([+]|[00]{2})([0-9]|[ -])*/','min:12','max:14'],
                'phone' => ['required','regex:/^([+]|[00]{2})([0-9]|[ -])*/','min:12','max:14']
            ];
        }
        return $rules;
    }
}
