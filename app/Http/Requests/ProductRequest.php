<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // dd($this->request->all());
        $rules =  [
            'title' => 'required',
            'description' => 'required',
            'image' => 'image',
            'quantity'=>'required|integer',
            'price'=>'required|integer|min:1|digits_between:1,6',
        ];
        return $rules;
    }
}
