<?php


namespace App\Traits;


use App\Models\{ProductImage,Product,Category,OrderDetail,ReviewImages,Review};
use App\Models\User;
use Illuminate\Support\Facades\File;
use Carbon\Carbon;
use App\Jobs\ProductDiscount;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Libraries\Uploader;

trait Products
{

    public function allProducts()
    {
        $products = Product::orderBy('created_at', 'DESC')->with(['reviews'])->paginate(8);
        foreach($products->items() as $product){
            $product->average_rating = $product->reviews->avg('rating');
            if($product->average_rating == null){
                $product->average_rating = 0;
            }
            $product->discountedPrice = getDiscountPriceObject($product->price, $product->discount);
            $product->price = getPriceObject($product->price);
        }
        return $products;
    }

    public function productDetail($id, $slug = "", $fromWeb = false)
    {
        $product_id = $id;
        if($fromWeb){
            $product = $this->getIdFromSlug($slug);
            if(!$product){
                return false;
            }
            $product_id = $product;
        }

        $reviews = function($query){
            $query->select('id', 'user_id', 'supplier_id', 'product_id',  'order_detail_id', 'comment', 'rating', 'created_at')->with('user:id,image,first_name,last_name')->orderBy('created_at', 'desc')->limit(5);
        };    
        $select = ['id','user_id','slug','title','price','discount','quantity','image','expiry_date','minimum_order','description','is_refundable'];
        $product = Product::select($select)->with(['images:id,product_id,image,default_image','supplier:id,supplier_name','reviews' => $reviews])->findOrFail($product_id);
        $product->discountedPrice = getDiscountPriceObject($product->price, $product->discount);
        $product->price = getPriceObject($product->price);
        $product->average_rating = $product->reviews->avg('rating');
        if($product->average_rating == null){
            $product->average_rating = 0;
        }
        $product->can_review = "";
        return $product;
    }

    public function canReview($product, $fromWeb = false)
    {
        if($fromWeb != false){
            $user = $this->user;
        }
        else{
            $user = \request('jwt.user', new \stdClass());
        }
        if(isset($user->id)){
            $product->load(['orderDetails' => function($q) use($user){ $q->where('is_review_product', 0)->where('user_id', $user->id)->where('item_status', 'delivered'); }]);
            if($product->orderDetails->count() > 0)
            {
                foreach($product->orderDetails as $detail)
                {
                    $product->can_review = $detail->id;
                    break;
                }
            }
            unset($product->orderDetails);
        }
        return $product;
    }

    public function productSearch($request)
    {
        $query = Product::query();
        $select = ['title','price', 'slug','discount', 'quantity', 'latitude','longitude','id','user_id', 'image'];
        if ($request->input()) {
            $radiusSearch = false;
            $lat = $request->get('latitude', 0);
            $long = $request->get('longitude', 0);
            // dd($long);
            $distance = config("settings.nearby" , 10); //km

            if ($request->has('keyword') && $request->keyword !='') {
                $query->where('title', 'like', '%"'.$request->keyword.'"%');
            }

            if ($request->has('categories')){
                $checkCategories = function ($query) use ($request) {
                    $query->whereIn('category_id', $request->categories);
                };
                $query->whereHas('categories',$checkCategories);
            }

            if ($request->has('category') && $request->category != 'all'){
                $categories = Category::where('parent_id', $request->category)->select('id')->get();
                $categories_array = [];
                foreach($categories as $category){
                    array_push($categories_array, $category->id);
                }
                $checkCategories = function ($query) use ($request, $categories_array) {
                    $query->whereIn('category_id', $categories_array);
                };
                $query->whereHas('categories',$checkCategories);
            }

            if ($request->has('supplier') && $request->supplier !='') {
                $checkSupplier = function($query) use ($request){
                    $query->where('supplier_name', 'like', '%"'.$request->supplier.'"%');
                };
                $query->whereHas('supplier', $checkSupplier);
            }

            if($request->has('min_price') > 0)
            {
               
                $minPrice = $request->min_price;
                if($request->has('currency') && $request->currency == 'usd')
                {
                    $rate = getConversionRate();
                    $minPrice = $minPrice * $rate;
                }
                $query->where('price', '>=', $minPrice);
            }

            if($request->has('max_price') > 0)
            {
                $maxPrice = $request->max_price;
                if($request->has('currency') && $request->currency == 'usd')
                {
                    $rate = getConversionRate();
                    $maxPrice = $maxPrice * $rate;
                }
                $query->where('price', '<=', $maxPrice);
            }

            if ($lat > 0 && $long >0)
            {
                $radiusSearch = true;
            }
            if ($radiusSearch) {
                $haversine  = '( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$long.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )';
                $products = $query->select($select)->selectRaw(" {$haversine} AS distance")->whereRaw("{$haversine} < ?", [$distance])->with(['supplier:id,supplier_name','reviews'])->orderBy('created_at', 'DESC')->paginate(8);
                foreach($products as $product){
                    $product->discountedPrice = getDiscountPriceObject($product->price, $product->discount);
                    $product->price = getPriceObject($product->price);
                    $product->average_rating = $product->reviews->avg('rating');
                    if($product->average_rating == null){
                        $product->average_rating = 0;
                    }
                }
                return $products; 
               
            }
            else{
                $products = $query->select($select)->orderBy('created_at', 'DESC')->with(['supplier:id,supplier_name','reviews'])->paginate(8);
            }
        }
        else{
            $products = $query->select($select)->orderBy('created_at', 'DESC')->with(['supplier:id,supplier_name','reviews'])->paginate(8);
        }    

        foreach($products->items() as $product){
            $product->discountedPrice = getDiscountPriceObject($product->price, $product->discount);
            $product->price = getPriceObject($product->price);
            $product->average_rating = $product->reviews->avg('rating');
            if($product->average_rating == null){
                $product->average_rating = 0;
            }
        }
        return $products;
    }

    public function addProductTrait($request, $fromWeb = false){
        // dd($request->all());
        if ($request->product_id > 0){
            ProductImage::where('product_id',$request->product_id)->delete();
        }
        if ($fromWeb){
            $user = $this->user;
        }else{
            $user = \request('jwt.user', new \stdClass());
        }

        if($user->is_supplier != 1)
        {
            return 'unauthorized';
        }
        
        if($request->product_id > 0)
        {
            $product = Product::find($request->product_id);
            if($product->user_id != $user->id)
            {
                return 'unauthorized';
            }
        }
        $data = $request->only([
            'price', 'discount', 'quantity', 'minimum_order', 'is_refundable'
        ]);
        $data['latitude']  = $user->latitude;
        $data['longitude']  = $user->longitude;
        $data['address']  = $user->address;
        $data['city_id']  = $user->city_id;
        if($request->product_id == 0){
            $data['title->en'] = '';
            $data['title->ar'] = '';
            $data['description->en'] = '';
            $data['description->ar'] = '';
        }
        
        if($fromWeb){
            $data['title->en'] = $request->title_en;
            $data['title->ar'] = $request->title_ar;
            $data['description->en'] = $request->description_en;
            $data['description->ar'] = $request->description_ar;
            if($request->has('title_en') && $request->title_en != '')
            {
                $data['slug']= str_slug($request->title_en);
                $productExist =  Product::where('slug',$data['slug'])->first();
                if ($productExist !==  null){
                    $data['slug'] = $this->incrementSlug($data['slug'],$request->product_id);
                }
            }
            if ($request->has('images') &&  $request->default_image == ''){
                $images = json_decode($request->images);
                if(sizeof($images) > 0){
                    $data['image'] = $images[0][0];
                }
            }
        }
        else{
            if($request->has('description'))
            {
                if(array_key_exists('en', $request->description))
                {
                    $data['description->en'] = $request->description['en'];
                }
                if(array_key_exists('ar', $request->description))
                {
                    $data['description->ar'] = $request->description['ar'];
                }
            }
            if(array_key_exists('en', $request->title))
            {
                $data['title->en'] = $request->title['en'];
                $data['slug']= str_slug($request->title['en']);
                $productExist =  Product::where('slug',$data['slug'])->first();
                if ($productExist !==  null){
                    $data['slug'] = $this->incrementSlug($data['slug'],$request->product_id);
                }
            }
            if(array_key_exists('ar', $request->title))
            {
                $data['title->ar'] = $request->title['ar'];
            }
            if ($request->has('images') &&  $request->default_image == ''){
                $data['image'] = $request->images[0];
            }
        }
        if($request->has('expiry_date') && $request->expiry_date != '')
        {
            $data['expiry_date'] = Carbon::parse($request->expiry_date)->timestamp;
        }
        
        if ($request->has('default_image') && $request->default_image != '') {
            $data['image'] = $request->get('default_image');
        }
        
        $data['user_id'] = $user->id;
        $product = Product::updateOrCreate(['id' => $request->product_id],$data);
        $product->categories()->sync($request->category_id);

        if($product->expiry_date != NULL)
        {
            $date = Carbon::now()->toDateString();
            $date = carbon::parse($date)->timestamp;
            $expiry_time = $product->expiry_date - $date  ;
            if ($expiry_time >= 0 && $product->discount > 0){
                $finish_date = Carbon::parse($request->expiry_date)->addDay(1);
                $start_date =   Carbon::now()->toDateString();
                $delay = Carbon::parse($finish_date)->diffInSeconds($start_date);
                ProductDiscount::dispatch($product->id)->delay($delay);
            }
        }


        if ($request->has('images')) {
            $uploadImages = [];
            if($fromWeb){
                $imagesArray = json_decode($request->images);
                    foreach ($imagesArray as $key => $value){
                        $default = '0';
                        if ($request->default_image != '' && $request->default_image == $value[0]){
                            $default = '1';
                        }
                        $uploadImages[] = ['product_id' => $product->id, 'image' => $value[0],'default_image' => $default,'type' => 'interior'];
                    }
            }
            else{
                foreach ($request->images as $key => $value) {
                    $default = '0';
                    if ($request->default_image != '' && $request->default_image == $value) {
                        $default = '1';
                    }
                    $uploadImages[] = ['product_id' => $product->id, 'image' => $value,'default_image' => $default,];
                }
            }
            ProductImage::insert($uploadImages);       
        }
        return 'success';
    }

    

    public function incrementSlug($slug ,$id) {

        $original = $slug;

        $count = 2;
        if ($id == 0){
            while (Product::whereSlug($slug)->exists()) {

                $slug = "{$original}-" . $count++;
            }
        }
        else{
            while (Product::whereSlug($slug)->where('id','!=',$id)->exists()) {

                $slug = "{$original}-" . $count++;
            }
        }



        return $slug;

    }

    public  function deleteProduct($request , $fromWeb = false){
        if ($fromWeb){
            $user= $this->user;
        }
        else{
            $user = \request('jwt.user', new \stdClass());
            $token = \request('jwt.token', new \stdClass());
        }
        $product = Product::findOrFail($request->product_id);
      if ( $user->id !== $product->user_id){
          return false;
      }
      $product->images()->delete();
      $product->delete();
      return true;
    }

    public function searchProductTrait($request , $fromWeb = false){
            $query = Product::query();
        $select = ['name','price','latitude','longitude','id','user_id','image',\DB::raw('concat("' . url('/') . '/",image)as image')];
        $selectStore = function ($query){
            $query->select('id','type','chef_name');
        };
        if ($request->input()) {
                $radiusSearch = false;
                $lat = $request->get('latitude', 0);
                $long = $request->get('longitude', 0);
                $distance = config("settings.nearby" , 10); //km
                if ($request->has('keyword') && $request->keyword !='') {
                    $query->where('title->en', $request->keyword);
                }
                $checkCategories = function ($query) use ($request) {
                    $query->where('category_id', $request->category);
                };
                if ($request->category){
                    $query->whereHas('categories',$checkCategories);
                }
                if ($request->sub_category){
                    $categoriesArray = [$request->category, $request->sub_category];
                    $query->whereHas('categories', function ($q) use ($categoriesArray) {
                        $q->whereIn('id', $categoriesArray);
                    }, '=', count($categoriesArray));
                }
                if ($request->has('store') && $request->store !='') {
                    $checkStore = function($query) use ($request){

                        $query->where('chef_name->en', $request->store);
                    };
                    $query->whereHas('store', $checkStore);
                }

                if ($request->price_range){
                    $price_range = explode(';',$request->price_range);
                   if(!isset($price_range[1])){
                       $price_range[1] = 10000000;
                   }
                    if($request->has('currency') && $request->currency == 'usd')
                    {
                        $rate = getConversionRate();
                        $price_range[0] = $price_range[0] * $rate;
                        $price_range[1] = $price_range[1] * $rate;
                    }
                    $query->whereBetween('price',[$price_range[0],$price_range[1]]);
                }
                if ($lat > 0 && $long >0)
                {
                    $radiusSearch = true;
                }
                if ($radiusSearch) {
                    $haversine  = '( 6367 * acos( cos( radians('.$lat.') )* cos( radians( latitude ) ) *cos( radians( longitude ) - radians('.$long.') ) + sin( radians('.$lat.') )* sin( radians( latitude ) ) ) )';
                    $products = $query->select($select)->selectRaw(" {$haversine} AS distance")->whereRaw("{$haversine} < ?", [$distance])->with(['store'=>$selectStore])->orderBy('created_at', 'DESC')->paginate(10);
                }
                else{
                    $products = $query->select($select)->with(['store'=> $selectStore])->orderBy('created_at', 'DESC')->paginate(20);
                }
                foreach($products as $product){
                    $product->price = getPriceObject($product->price);
                }
                return $products;
            }
            else{
                $products = $query->select($select)->orderBy('created_at', 'DESC')->paginate(20);
                foreach($products as $product){
                    $product->price = getPriceObject($product->price);
                }
                return $products;
            }
        }

        public function imageUpload($request)
        {
            $imageUploadedPath = '';
            if ($request->hasFile('image')) {
                $uploader = new Uploader('image');
                if ($uploader->isValidFile()) {
                    $uploader->upload('media', $uploader->fileName);
                    if ($uploader->isUploaded()) {
                        $imageUploadedPath = $uploader->getUploadedPath();
                    }
                }
                if (!$uploader->isUploaded()) {
                    return responseBuilder()->error(__('Something went Wrong'));
                }
            }
            $data['file_name'] = $imageUploadedPath;
            $data['file_path'] = url($imageUploadedPath);
            return $data;
        }

        public function getCategories()
        {
            $categories = Category::where('parent_id', 0)->with('subCategories')->select('id','parent_id','title','image')->get();
            return $categories;
        }

        public function destroyProduct($id)
        {
            $user = \request('jwt.user', new \stdClass());
            $product = Product::findOrFail($id);
            if($product->user_id != $user->id)
            {
                return 'unauthorized';
            }
            $product = Product::destroy($id);
            return $product;
        }


        public function productsByCategory($id)
        {
            $categories = function($categories) use($id){
                $categories->where('category_id', $id);
            };
            $products = Product::whereHas('categories', $categories)->with(['reviews','supplier:id,supplier_name'])->paginate(8);
            foreach($products->items() as $product){
                $product->average_rating = $product->reviews->avg('rating');
                if($product->average_rating == null){
                    $product->average_rating = 0;
                }
                $product->discountedPrice = getDiscountPriceObject($product->price, $product->discount);
                $product->price = getPriceObject($product->price);
            }
            return $products;
        }

        public function productsBySupplier($id)
        {
            $supplier = function($supplier) use($id){
                $supplier->where('user_id', $id);
            };
            $products = Product::whereHas('supplier', $supplier)->with(['reviews','supplier:id,supplier_name'])->paginate(8);
            foreach($products->items() as $product){
                $product->average_rating = $product->reviews->avg('rating');
                if($product->average_rating == null){
                    $product->average_rating = 0;
                }
                $product->discountedPrice = getDiscountPriceObject($product->price, $product->discount);
                $product->price = getPriceObject($product->price);
            }
            return $products;
        }

        public function getIdFromSlug($slug)
        {
            $product = Product::where('slug', $slug)->first();
            if($product){
                return $product->id;
            }
            return false;
        }

        public function reviewProductTrait($request, $fromWeb = false)
        {
            if ($fromWeb){
                $user= $this->user;
            }
            else{
                $user = \request('jwt.user', new \stdClass());
            }


            if($user->is_supplier == 1)
            {
                return 'unauthorized';
            }
            $checkUser = OrderDetail::where('user_id', $user->id)->where('id', $request->order_detail_id)->where('is_review_product', 0)->where('item_status', 'delivered')->first();
            if(!$checkUser)
            {
                return 'unauthorized';
            }
            $data = $request->only('rating', 'order_detial_id');
            $data['comment'] = $request->review;
            $data['user_id'] = $user->id;
            $data['order_detail_id'] = $request->order_detail_id;
            $data['product_id'] = $checkUser->product_id;
            $review = Review::create($data);
            if ($request->has('images')) {
                $uploadImages = [];
                foreach ($request->images as $key => $value) {
                    $uploadImages[] = ['review_id' => $review->id, 'image' => $value];
                }
                ReviewImages::insert($uploadImages);
            
            }
            if(!$review)
            {
                return false;
            }
            OrderDetail::where('id', $request->order_detail_id)->update(['is_review_product' => 1]);
            return $review;
        }


}