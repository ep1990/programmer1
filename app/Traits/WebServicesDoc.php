<?php

namespace App\Traits;

use App\Models\WebService;

trait WebServicesDoc {

    public function urlComponents($title, $params, $responseSample){
        $webServicesDoc = [];
        $webServicesDoc['title'] = $title;
        $currentMiddleware = request()->route()->computedMiddleware;
        foreach ($currentMiddleware as $key=>$middleware){
            if (!is_object($middleware)){
                $webServicesDoc['auth'] = ($middleware == "jwt.auth")? 'yes': 'no';
            }
        }
        $webServicesDoc['method_name'] = \Route::getCurrentRoute()->getActionMethod();
        $webServicesDoc['url'] = str_replace(config('app.locale').'/api/', '', request()->path());
        $webServicesDoc['method'] = request()->method();
        $webServicesDoc['params'] = json_encode($params);
        $webServicesDoc['response_sample'] = $responseSample->getContent();
        WebService::updateOrCreate([
            'method_name'=>$webServicesDoc['method_name'],
//            'url'=>$webServicesDoc['url'],
            'method'=>$webServicesDoc['method']],$webServicesDoc);
        return $webServicesDoc;
    }
}