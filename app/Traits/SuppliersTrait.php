<?php


namespace App\Traits;


use App\Models\Notification;
use App\Models\User;
use App\Models\{Product,Review,OrderDetail,ReviewImages};
use http\Env\Request;
use Illuminate\Support\Facades\DB;
use function foo\func;
use function GuzzleHttp\Promise\all;

trait SuppliersTrait
{

    public function searchTrait($request, $fromWeb = false)
    {
        $radiusSearch = false;
        $lat = $request->get('latitude', 0);
        $long = $request->get('longitude', 0);
        $distance = config("settings.nearby" , 10); //km
        $select = ['id', 'supplier_name', 'average_rating', 'image','latitude','longitude','address'];
        $query = User::where(['is_supplier' => 1, 'is_verified' => 1, 'is_active' => 1]);
        if ($request->has('keyword') && $request->keyword !='') {
            // $query->where('kiosk_name->en', $request->keyword);
            $query->where('supplier_name', 'like', '%"'.$request->keyword.'"%');
        }
        if ($request->has('city_id') && $request->city_id !='') {
            $query->where('city_id', $request->city_id);
        }
        
        if ($lat > 0 && $long >0)
        {
            $haversine  = '( 6367 * acos( cos( radians('.$lat.') )* cos( radians( latitude ) ) *cos( radians( longitude ) - radians('.$long.') ) + sin( radians('.$lat.') )* sin( radians( latitude ) ) ) )';
            $query->select($select)->selectRaw(" {$haversine} AS distance")->whereRaw("{$haversine} < ?", [$distance]);

            if($request->has('sort_order') && $request->sort_order == 'near_to_far'){
                $query->orderBy('distance', 'asc');
            }
            if($request->has('sort_order') && $request->sort_order == 'far_to_near'){
                $query->orderBy('distance', 'desc');
            }

            // $suppliers = $query->orderBy('distance', 'asc')->get();        
            // $select[] = \DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$long.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) ) AS distance');
            // $radiusSearch = true;
            $suppliers = $query->paginate(6);
            return $suppliers;
        }
        // if ($radiusSearch) {
        //     if($request->has('sort_order') && $request->sort_order == 'near_to_far'){
        //         $query->orderBy('distance', 'asc');
        //     }
        //     if($request->has('sort_order') && $request->sort_order == 'far_to_near'){
        //         $query->orderBy('distance', 'desc');
        //     }
        //     $suppliers = $query->select($select)->having('distance', '<=', $distance)->orderBy('distance', 'asc')->get();
            
        // }
        else{
            if($fromWeb)
            {
                if(session('CITY_ID') != null){
                    $query->where('city_id', session('CITY_ID'));
                }
            }
            if($request->has('sort_order')){
                if($request->sort_order == 'top_rated'){
                    $query->orderBy('average_rating', 'desc');
                }
                if($request->sort_order == 'a_to_z'){
                    $query->orderBy('supplier_name', 'asc');
                }
            }
            $suppliers = $query->select($select)->paginate(6);
        }
        return $suppliers;
    }

    public function detailTrait($request)
    {
        $reviews = function($reviews){
            $reviews->select('id','user_id','supplier_id','rating','comment','created_at')->with('user:id,first_name,last_name,image','images:id,review_id,image')->orderBy('created_at', 'desc')->limit(5);
        };
        $supplier = User::select('id','supplier_name','address','email','phone_number',\DB::raw('concat("' . url('/') . '/",trade_license)as trade_license'),'image','is_verified','latitude','longitude','average_rating','description','fcm_token','is_notification')
            ->with(['reviewsSupplier' => $reviews])
            ->findOrFail($request->supplier_id);
        return $supplier;
    }

    public function canReview($supplier, $fromWeb = false)
    {
        if($fromWeb){
            $user = $this->user;
        }
        else{
            $user = \request('jwt.user', new \stdClass());
        }
        if(isset($user->id)){
            $supplier->load(['orderDetails' => function($q) use($user){ $q->where('is_review_supplier', 0)->where('user_id', $user->id)->where('item_status', 'delivered'); }]);
            if($supplier->orderDetails->count() > 0)
            {
                foreach($supplier->orderDetails as $detail)
                {
                    $supplier->can_review = $detail->id;
                    break;
                }
            }
            unset($supplier->orderDetails);
        }
        return $supplier;
    }

    public function productsTrait($request)
    {
    
        $products = Product::select('id','user_id','image','discount','price','title')
            ->where('user_id', $request->supplier_id)->with(['rating:id,rating,product_id'])->paginate(8);
        foreach($products->items() as $product){
            $product->average_rating = $product->rating->avg('rating');
            if($product->average_rating == null){
                $product->average_rating = 0;
            }
            $product->discountedPrice = getDiscountPriceObject($product->price, $product->discount);
            $product->price = getPriceObject($product->price);
        }
        return $products;
    }

    public function reviewSupplierTrait($request, $fromWeb = false)
    {
        if($fromWeb){
            $user = $this->user;
        }
        else{
            $user = \request('jwt.user', new \stdClass());
        }

        if($user->is_supplier == 1)
        {
            return 'unauthorized';
        }
        $checkUser = OrderDetail::where('user_id', $user->id)->where('id', $request->order_detail_id)->where('is_review_supplier', 0)->where('item_status', 'delivered')->first();
        if(!$checkUser)
        {
            return 'unauthorized';
        }
        $data = $request->only('rating', 'order_detial_id');
        $data['comment'] = $request->review;
        $data['user_id'] = $user->id;
        $data['supplier_id'] = $checkUser->supplier_id;
        $data['order_detail_id'] = $request->order_detail_id;
        $review = Review::create($data);
        if ($request->has('images')) {
            $uploadImages = [];
            foreach ($request->images as $key => $value) {
                $uploadImages[] = ['review_id' => $review->id, 'image' => $value];
            }
            ReviewImages::insert($uploadImages);
           
        }
        if(!$review)
        {
            return false;
        }
        OrderDetail::where('id', $request->order_detail_id)->update(['is_review_supplier' => 1]);
        return $review;
    }
}