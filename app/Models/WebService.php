<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Apr 2018 05:47:07 +0000.
 */

namespace App\Models;

//use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Class WebService
 * 
 * @property int $id
 * @property string $title
 * @property string $auth
 * @property string $url
 * @property string $method
 * @property string $params
 * @property string $response_sample
 * @property string $method_name
 * @property int $created_at
 * @property int $updated_at
 *
 * @package App\Models
 */
class WebService extends Model
{
	use \App\Models\CommonModelFunctions;
	protected $dateFormat = 'U';
	public static $snakeAttributes = false;

	protected $casts = [
		'created_at' => 'int',
		'updated_at' => 'int'
	];

	protected $fillable = [
		'title',
		'auth',
		'url',
		'method',
		'params',
		'response_sample',
		'method_name'
	];
}
