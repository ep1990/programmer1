<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 17 Jan 2019 13:47:24 +0000.
 */

namespace App\Models;

//use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Review
 * 
 * @property int $id
 * @property int $admin_id
 * @property string $polis
 * @property float $credit_limit
 * @property float $opening_balance
 * @property float $invoice_amount
 * @property float $order_amount
 * @property string $remarks
 * @property string $status
 * @property int $created_at
 * @property int $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $orders
 *
 * @package App\Models
 */
class Review extends Model
{
    use CommonFunctions;
	use \Illuminate\Database\Eloquent\SoftDeletes;
	use \App\Models\CommonModelFunctions;
	protected $dateFormat = 'U';
	public static $snakeAttributes = false;

	protected $casts = [
		'created_at' => 'int',
        'updated_at' => 'int',
        'deleted_at' => 'int'
	];

	protected $fillable = [
		'user_id',
		'supplier_id',
        'order_detail_id',
        'product_id',
		'image',
        'comment',
        'rating'
	];

    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function supplier()
    {
        return $this->belongsTo(User::class, 'supplier_id');
    }

    public function orderDetail()
    {
        return $this->belongsTo(Order::class);
    }

    public function products()
    {
        return $this->belongsTo(Product::class);
    }

    public function images()
    {
        return $this->hasMany(ReviewImages::class);
    }
}
