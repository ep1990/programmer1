<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 05 Nov 2018 11:27:25 +0000.
 */

namespace App\Models;

//use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductImage
 * 
 * @property int $id
 * @property int $product_id
 * @property string $image
 * @property bool $is_default
 * @property int $created_at
 * @property int $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\Product $product
 *
 * @package App\Models
 */
class ReviewImages extends Model
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	use \App\Models\CommonModelFunctions;
	use CommonFunctions;
	protected $dateFormat = 'U';
	public static $snakeAttributes = false;
	protected $casts = [
		'product_id' => 'int',
		'created_at' => 'int',
		'updated_at' => 'int'
	];
	protected $fillable = [
		'review_id',
        'image',
	];
	public function review()
	{
		return $this->belongsTo(\App\Models\Review::class);
	}
}
