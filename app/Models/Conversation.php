<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dateFormat = 'U';
    protected $fillable = ['id','product_id'];

    public function conversationUsers()
    {
        return $this->belongsToMany(User::class)->withPivot('user_id');
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function latestMessage()
    {
        return $this->hasOne(Message::class)->latest();
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
