<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 08 Jan 2019 05:54:24 +0000.
 */

namespace App\Models;

//use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductLabel
 * 
 * @property int $product_id
 * @property int $label_id
 * 
 * @property \App\Models\Label $label
 * @property \App\Models\Product $product
 *
 * @package App\Models
 */
class ProductLabel extends Model
{
	use \App\Models\CommonModelFunctions;
	protected $table = 'product_label';
	public $incrementing = false;
	public $timestamps = false;
	protected $dateFormat = 'U';
	public static $snakeAttributes = false;

	protected $casts = [
		'product_id' => 'int',
		'label_id' => 'int'
	];

	protected $fillable = [
		'product_id',
		'label_id'
	];

	public function label()
	{
		return $this->belongsTo(\App\Models\Label::class);
	}

	public function product()
	{
		return $this->belongsTo(\App\Models\Product::class);
	}
}
