<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    use \App\Models\CommonModelFunctions;
    protected $dateFormat = 'U';
    public static $snakeAttributes = false;

    protected $fillable = ['id','question->en','question->ar','answer->en','answer->ar'];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    protected $casts = [
        'created_at' => 'int',
        'updated_at' => 'int',
        'question' => 'array',
        'answer' => 'array'
    ];


    public function languages()
    {
        return $this->belongsToMany(\App\Models\Language::class)
            ->withPivot('question', 'answer');
    }
}
