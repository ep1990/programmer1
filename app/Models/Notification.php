<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 18 Dec 2018 05:28:00 +0000.
 */

namespace App\Models;

//use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Model;
use App\Observers\NotificationObserver;
use Request;

/**
 * Class Notification
 * 
 * @property int $id
 * @property int $user1_id
 * @property int $user2_id
 * @property int $sender_id
 * @property string $title
 * @property string $description
 * @property int $extras
 * @property string $action
 * @property bool $is_seen
 * @property int $created_at
 * @property int $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Notification extends Model
{

	use \Illuminate\Database\Eloquent\SoftDeletes;
	use \App\Models\CommonModelFunctions;
	protected $dateFormat = 'U';
	public static $snakeAttributes = false;
    protected $table = 'notifications';
	protected $casts = [
		'created_at' => 'int',
		'updated_at' => 'int',
		'title' => 'array',
		'description' => 'array',
		'extras' => 'array'
	];

	protected $fillable = [
		'sender_id',
		'receiver_id',
		'action',
		'fcm_token',
		'title->en',
		'title->ar',
		'description->en',
		'description->ar',
		'extras',
		'type',
		'is_seen',
		'is_read'
	];

	public function sender()
	{
		return $this->belongsTo(User::class, 'sender_id');
	}

	public function receiver()
	{
		return $this->belongsTo(User::class, 'receiver_id');
	}
    public function languages(){
        return $this->belongsToMany('App\Models\Language')->withPivot('title','description');
	}

	
	
	
//    public function delete(){
//	    dd('hello');
//        $this->languages()->delete();
//        return parent::delete();
//    }
//    public static function boot() {
//        parent::boot();
//        static::deleting(function($notification) { // before delete() method call this
//            $notification->languages()->delete();
//            // do the rest of the cleanup...
//        });
//    }
}
