<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 16 Jan 2019 13:38:38 +0000.
 */

namespace App\Models;

//use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	use \App\Models\CommonModelFunctions;
	use CommonFunctions;
	protected $dateFormat = 'U';
	public static $snakeAttributes = false;

	protected $casts = [
//		'price' => 'float',
		'quantity' => 'int',
		'discount' => 'float',
		'created_at' => 'int',
		'updated_at' => 'int',
	];

	protected $fillable = [
        'slug',
        'title',
        'description',
		'price',
		'quantity',
		'image',
		'discount',
	];
    public function categories(){
	   return  $this->belongsToMany(Category::class);
	}
	

	public function images(){
        return $this->hasMany(ProductImage::class);
    }


    

}
