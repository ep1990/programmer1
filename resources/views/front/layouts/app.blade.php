
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="Lnwjav0d2mulh3anG78G6wASpt5I3tBWG1eT5vMW">

    <title>Cart-System</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<style>
    .span{

    }
    .badge-info{
            box-shadow: 2px 6px 5px 0px #0303;
            cursor: pointer;
    }
    .badge-success{
            box-shadow: 2px 6px 5px 0px #0303;
            cursor: pointer;
    }
    .badge-primary{
            box-shadow: 2px 6px 5px 0px #0303;
            cursor: pointer;
    }
    .badge-danger{
            box-shadow: 2px 6px 5px 0px #0303;
            cursor: pointer;
    }
    .badge-warning{
            box-shadow: 2px 6px 5px 0px #0303;
            cursor: pointer;
    }
    .card{
        box-shadow: 21px 27px 24px -22px #0303;
    }
    .custom-message{
        position: fixed;
        top: 80px;
        right: 10px;
        width: 36%;
        height: 81px;
        line-height: 3;
        box-shadow: 5px 11px 11px 0px #0303;
        z-index: 100;
        display: none;
    }
    .dropdown:hover .dropdown-menu {
        display: block;
        margin-top: 0; // remove the gap so it doesn't close
    }
    .checkbox-span{
        cursor: pointer;
    }
    #overlay{	
            position: fixed;
            top: 0;
            z-index: 100;
            width: 100%;
            height:100%;
            display: none;
            background: rgba(0,0,0,0.6);
        }
        .cv-spinner {
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;  
        }
        .spinner {
            width: 40px;
            height: 40px;
            border: 4px #ddd solid;
            border-top: 4px #2e93e6 solid;
            border-radius: 50%;
            animation: sp-anime 0.8s infinite linear;
        }
        @keyframes sp-anime {
            100% { 
                transform: rotate(360deg); 
            }
        }
        .is-hide{
            display:none;
        }
</style>
<body>
<div id="overlay">
        <div class="cv-spinner">
            <span class="spinner"></span>
        </div>
    </div>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm" style="border-bottom:solid 1px #0303">
            <div class="container-fluid">
                <a class="navbar-brand" href="{{ route('front.index') }}">
                    Cart-System
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        @if($user != NULL)
                            @if($user->is_supplier == 1)
                                <a class="nav-link" href="javascript:void(0)">{{ $user->supplier_name}}</a>
                            @else
                                <a class="nav-link" href="{{ route('front.cart.index') }}">Cart <span class="badge badge-success">{{session('cart')}}</span></a>
                                <a class="nav-link" href="javascript:void(0)">{{ $user->first_name.' '.$user->last_name}}</a>
                            @endif    
                                <a class="nav-link" href="{{route('front.auth.logout')}}" >Logout</a>
                        @else
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('front.auth.login') }}">Login</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('front.auth.register') }}">Register</a>
                            </li>
                        @endif    
                    </ul>
                </div>
            </div>
        </nav>
        @yield('content')
    </div>
    <!-- Scripts -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script><script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
    <script>
        window.Laravel = {!! json_encode([
                    'csrfToken' => csrf_token(),
                    'baseUrl' => url('/').'/'.config('app.locale')."/"/*url(config('app.locale')).'/'*/,
                    'apiUrl' => url('/').'/'.config('app.locale')."/api/"/*url(config('app.locale')).'/'*/,
                    'base' => url('/').'/',
                    'locale' => config('app.locale'),
                    'user_id' => $user['id'],
                    'currency' => $currency,
                    //  'authorization' => $user['token']

                ]) !!};
    </script>
    @yield('js')
</body>
</html>


