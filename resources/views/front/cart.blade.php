@extends('front.layouts.app')

@section('css')

<style>
    .card {
        height: 400px;
        margin-top: 20px !important;
        position: relative;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: none;
        border-radius: 0.25rem;
        position: relative;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: none;
        border-radius: 0.25rem;
    }
</style>

@endsection

@section('content')

<section class="sec-our-categories" id="app" >
        <div class="container">
            <div class="row" v-if="cart">
                  <div class="col-lg-12">
                      <div class="my-ordemt-a mt-3 mt-md-4 consume-cart-card" v-for="(data, index) in cartData">
                          <div class="row border-bottom ">
                              <div class="to-main-cart-cl">
                                  <div class="order-img d-flex ">
                                      <img v-bind:src=data.product.image alt="" class="img-fluid" i18n-alt style="height:171px; width:222px">
                                  </div>
                                  <div class="inner-car-cl">
                                      <div class="order-text">
                                          <h5 class="tittle-order" i18n>@{{data.product.title }}</h5>
                                          <ul
                                              class="d-flex  justify-content-center justify-content-md-start  order-combination">


                                              <form class="main-cart-us" method="post" action="{{ route('front.cart.update') }}">
                                                  @csrf()
                                                  <input type="hidden" value="" name="product_id">
                                                  <div class="quantity-car-btn d-flex flex-wrap flex-md-nowrap">
                                                      <div
                                                          class="input-group increment-height mb-3 mb-xl-0 d-flex align-items-center mr-2">
                                                          <button type="button" class="btn quant-btn increment-height minus" @click="addMinusQuantity('minus', index, data.id)">
                                                              -
                                                          </button>
                                                          <input type="text" name="quantity" maxlength="3" min="0"  :id="'quantity-'+index" @change="addMinusQuantity('minus', index, data.id)" class="form-control input-number text-center increment-height" v-bind:value=data.quantity>
                                                          <button type="button" class="btn quant-btn increment-height add" @click="addMinusQuantity('add', index, data.id)">
                                                              +
                                                          </button>
                                                      </div>
                                                  </div>

                                              </form>
                                              <h5 class="mb-0  GOTHICB-1 amount combination-list w-100"> {{ __('total amount')}}
                                                  :&nbsp;<span class="color-primary">
                                                  @{{data.total_price}}</span>
                                              </h5>
                                          </ul>
                                      </div>
                                      <div class="chart-delete">
                                          <span @click="deleteProduct(data.id)">
                                              <span class="badge badge-danger">Delete</span>
                                          </span>
                                      </div>
                                  </div>


                              </div>
                          </div>

                      </div>

                  </div>

                <div class="col-lg-12">
                    <div class="my-ordemt-a mt-3 mt-md-4 consume-cart-card">
                        <div class="row border-bottom ">
                            <div class="delivery-info-chart-main">
                                <ul>
                                    <li class="main-tittle-li">{{__('sub total')}}: <span class="sub-tittle-chart-span">@{{ sub_total}}</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                

                <div class="col-lg-12">
                    <div class="cart-button-main d-flex flex-column">
                        <!-- <div class="user-profile-detail-mt">
                            <button type="button" class="btn btn-primary profile-btn orange-btn float-right"> {{ __('total price')}}: @{{ sub_total }}</button>
                          </div> -->
                          <div class="user-profile-detail-mt">
                            <br>
                            <a href="javascript:void(0)" class="btn btn-primary profile-btn float-right">Continue to Checkout</a>
                          </div>
                    </div>

                </div>


            </div>
            <div class="row" v-if="cart_empty">
                <div class="col-md-12 alert alert-danger">{{ __('No record found') }}</div>
            </div>
    </section>
@endsection

@section('js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js"></script>
    <script>
        $("#overlay").fadeIn();
        $('document').ready(function(){
            $('#app').fadeIn(300);
            $('#spinner').fadeOut(300);
            $("#overlay").fadeOut(300);
        })


        var app = new Vue({
            el: '#app',
            data: {
                cart: false,
                cart_empty: false,
                cartData: null,
                vat: null,
                sub_total: null,
                shipping_total: null,
                grand_total: null,
                coupon: true,
                coupon_applied: false,
                coupon_code: null,
                coupon_percent: 0,
                coupon_amount: 0,
                coupon_expire: false,
            },
            methods:{

                deleteProduct: function(id){
                    $("#overlay").fadeIn(300);
                    axios
                    .get(window.Laravel.baseUrl+'delete-cart/'+id)
                    .then(response => {
                        if(response.data.data.length > 0){
                            this.cart = true;
                            this.cartData = response.data.data;
                            this.vat = response.data.vat;
                            this.sub_total = response.data.sub_total;
                            this.shipping_total = response.data.shipping_total;
                            this.grand_total = response.data.grand_total;
                        }
                        else{
                            this.cart = false;
                            this.cart_empty = true
                        }
                    })
                    $("#overlay").fadeOut(300);
                },

                getCart: function(){
                    axios
                    .get(window.Laravel.baseUrl+'get-cart-data')
                    .then(response => {
                        if(response.data.data.length > 0){
                            this.cart = true;
                            this.cartData = response.data.data;
                            this.sub_total = response.data.sub_total;
                        }
                        else{
                            this.cart = false;
                        }
                    })
                },

                addMinusQuantity: function(name, index, cart_id){
                    var self = this;
                    if(name == 'add'){
                        $('#quantity-'+index).val(parseInt($('#quantity-'+index).val())+parseInt(1));
                    }
                    else{
                        $('#quantity-'+index).val(parseInt($('#quantity-'+index).val())-parseInt(1));
                    }
                    
                    if(window.setTime){
                        clearTimeout(window.setTime);
                    }
                    
                    window.setTime = setTimeout(function(){
                        $("#overlay").fadeIn(300);
                        axios
                            .post(window.Laravel.baseUrl+'update-cart', {_token:window.Laravel.csrfToken, cart_id: cart_id, quantity: $('#quantity-'+index).val()})
                            .then(response => {
                                if(response.data.success == false){
                                    swal("", response.data.message, "warning");
                                }
                                self.getCart();
                            }) 
                            $("#overlay").fadeOut(300);
                    }, 1000);
                },

                couponApply: function(){
                    if(this.coupon_code == null){
                        swal("", 'Please enter coupon code', "warning");
                        return false;
                    }
                    $("#overlay").fadeIn(300);
                    axios
                    .get(window.Laravel.baseUrl+'valid-coupon/'+this.coupon_code)
                    .then(response => {
                        $("#overlay").fadeOut(300);
                        if(response.data.success == false){
                            swal("", response.data.message, "warning");
                        }
                        if(response.data.success == true){
                            swal("", response.data.message, "success");
                            this.coupon_code = null;
                            this.getCart();
                        }
                    })
                },

                couponRemove: function(){
                    var self = this;
                    $("#overlay").fadeIn(300);
                    axios
                    .get(window.Laravel.baseUrl+'remove-coupon/')
                    .then(response => {
                        $("#overlay").fadeOut(300);
                        if(response.data.success == false){
                            swal("", response.data.message, "warning");
                        }
                        if(response.data.success == true){
                            swal("", response.data.message, "success");
                            self.coupon_code = null;
                            self.coupon = true;
                            self.coupon_expire = false;
                            self.getCart();
                        }
                    })
                }
                
            },
            beforeMount(){
                this.getCart();
            },

        })
      
    </script>
@endsection