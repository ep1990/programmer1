@extends('front.layouts.app')

@section('content')

<main class="py-4">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">Register</div>
                            <div class="card-body">
                                <ul class="nav nav-tabs justify-content-center">
                                    <li class="active"><a class="btn btn-primary" data-toggle="tab" href="#user">As User</a></li>&nbsp;&nbsp;&nbsp;
                                    <li><a data-toggle="tab" class="btn btn-secondary" href="#supplier">As Supplier</a></li>
                                </ul><br></br>
                                @if(session()->has('error'))
                                    <p class="alert alert-danger">{{ session('error') }}</p>
                                @endif
                                @if( $errors->any())
                                    <ul class="alert alert-danger">
                                        @foreach($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                @endif
                                <div class="tab-content">
                                    <div id="user" class="tab-pane fade in active show">
                                        <form method="POST" action="{{route('front.auth.register')}}">
                                            @csrf()
                                            <input type="hidden" name="user_type" value="user">
                                            <div class="form-group row">
                                                <label for="email" class="col-md-4 col-form-label text-md-right">First Name</label>

                                                <div class="col-md-6">
                                                    <input id="text" type="text" class="form-control " name="first_name" value="{{ old('first_name') }}" required autocomplete="off" autofocus>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="email" class="col-md-4 col-form-label text-md-right">Last Name</label>

                                                <div class="col-md-6">
                                                    <input id="text" type="text" class="form-control " name="last_name" value="{{ old('last_name') }}" required autocomplete="off" autofocus>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>

                                                <div class="col-md-6">
                                                    <input id="email" type="text" class="form-control " name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>

                                                <div class="col-md-6">
                                                    <input id="password" type="password" class="form-control " name="password" required autocomplete="current-password">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="password" class="col-md-4 col-form-label text-md-right">Confirm Password</label>

                                                <div class="col-md-6">
                                                    <input id="password" type="password" class="form-control " name="password_confirmation" required autocomplete="current-password">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-0">
                                                <div class="col-md-8 offset-md-4">
                                                    <button type="submit" class="btn btn-primary">
                                                        Login
                                                    </button>

                                                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div id="supplier" class="tab-pane fade">
                                        <form method="POST" action="{{route('front.auth.register')}}">
                                            @csrf()
                                            <input type="hidden" name="user_type" value="supplier">
                                            <div class="form-group row">
                                                <label for="email" class="col-md-4 col-form-label text-md-right">Supplier Name</label>

                                                <div class="col-md-6">
                                                    <input id="text" type="text" class="form-control " name="supplier_name" value="{{ old('supplier_name') }}" required autocomplete="off" autofocus>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>

                                                <div class="col-md-6">
                                                    <input id="email" type="text" class="form-control " name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>

                                                <div class="col-md-6">
                                                    <input id="password" type="password" class="form-control " name="password" required autocomplete="current-password">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="password" class="col-md-4 col-form-label text-md-right">Confirm Password</label>

                                                <div class="col-md-6">
                                                    <input id="password" type="password" class="form-control " name="password_confirmation" required autocomplete="current-password">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-0">
                                                <div class="col-md-8 offset-md-4">
                                                    <button type="submit" class="btn btn-primary">
                                                        Login
                                                    </button>

                                                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>

@endsection