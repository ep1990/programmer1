@extends('admin.layouts.app')

@section('css')

<style>
    .card {
        height: 400px;
        margin-top: 20px !important;
        position: relative;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: none;
        border-radius: 0.25rem;
        position: relative;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: none;
        border-radius: 0.25rem;
    }
</style>

@endsection

@section('content')

<main class="py-4">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <div class="card">
                            <div class="card-header">
                                Add Product
                            </div>


                            <div class="card-body">
                            @if(session()->has('error'))
                                <p class="alert alert-danger">{{ session('error') }}</p>
                            @endif
                            @if($errors->any())
                                <ul class="alert alert-danger">
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif
                            <form method="POST" action="{{ route('admin.product-action', (isset($product->id))? $product->id : 0) }}" enctype="multipart/form-data">
                                    @csrf()
                                    <div class="form-group row">
                                        <label for="title" class="col-md-4 col-form-label text-md-right">Title</label>
                                        <div class="col-md-6">
                                            <input id="text" type="text" class="form-control " name="title" value="{{ (isset($product->id))? $product->title : old('email') }}" required autocomplete="off" autofocus>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="title" class="col-md-4 col-form-label text-md-right">Price</label>
                                        <div class="col-md-6">
                                            <input id="text" type="text" class="form-control " name="price" value="{{ (isset($product->id))? $product->price : old('price') }}" required autocomplete="off" autofocus>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="title" class="col-md-4 col-form-label text-md-right">Quantity</label>
                                        <div class="col-md-6">
                                            <input id="text" type="text" class="form-control " name="quantity" value="{{ (isset($product->id))? $product->quantity : old('quantity') }}" required autocomplete="off" autofocus>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="title" class="col-md-4 col-form-label text-md-right">Description</label>
                                        <div class="col-md-6">
                                            <textarea name="description" class="form-control">{{ (isset($product->id))? $product->description : old('description') }}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="title" class="col-md-4 col-form-label text-md-right">Image</label>
                                        <div class="col-md-6">
                                            <input  type="file" class="form-control product_upload_image_input" name="image" value="" autocomplete="off" autofocus>
                                            <input class="product-images-array" type="hidden" name="images" value='{{ old('product_image_array') ? 'product_image_array': json_encode($product->images) }}'>
                                        </div>
                                    </div>

                                    <div class="form-group m-form__group row image-product"></div>

                                    <div class="form-group row mb-0">
                                        <div class="col-md-8 offset-md-4">
                                            <button type="submit" class="btn btn-primary">
                                                {{(isset($product->id))? 'Update Product' : 'Add Product'}}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>

@endsection


@section('js')
    <script>
    var url = "{{ url('/').'/'.config('app.locale').'/' }}";
            let images = JSON.parse($(".product-images-array").val());
            console.log(images);
            let defaultImage = $(".defaultImage").val();

            function makeDefaut(image) {
                defaultImage = image;
                $(".defaultImage").val(defaultImage);
                apeendImaged();
            }

            function deleteImage(image) {
                $.ajax({
                    url: "{{ url('/en') }}/admin/delete-image",
                    type: "post",
                    data: {
                        'path': image
                    },headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                });
                console.log(images);
                let index = images.indexOf(image);
                console.log(image);
                images.splice(index, 1);
                apeendImaged();
            }


            function apeendImaged() {
                $(".image-product").empty();
                let imageDiv = ``;
                for (let image of images) {
                    // let imagePath = imageUrl(image[0], 120, 120, 100, 1);
                    let imagePath = "{{url('/')}}/"+image[0];
                    if (image[0] != defaultImage ) {
                        // imageDiv += `<div class='d-flex justify-cotent-center align-items-center'><img id='image' name='image' style='width:120px;height: 120px;' src='${imagePath}'></div> <a onclick='makeDefaut("${image}")'>default</a> <a onclick='deleteImage("${image}")'>delete</a>`
                        imageDiv += `<div class="col-3 border box-maker">
                                    <div class="height-width-set">
                                    <div class="button-groups d-flex justify-content-around">
                                        
                                        <button class="float-right left" onclick='deleteImage("${image[0]}")'><i class="fa fa-times"></i></button>
                                    </div>
                                    <img src="${imagePath}" class="img-fluid set-max-height-image">
                                    </div>
                                </div>`

                    }
                    else {
                    //

                        // imageDiv += `<div class='d-flex justify-cotent-center align-items-center'><img id='image' name='image' style='width:120px;height: 120px;' src='${imagePath}'></div> <a >defaulted</a>`
                        imageDiv += `<div class="col-3 border box-maker">
                                    <div class="height-width-set">
                                    <div class="button-groups d-flex justify-content-around">
                                        <button class="float-left right icon-active"><i class="fa fa-check"></i></button>
                                    </div>
                                    <img src="${imagePath}" class="img-fluid set-max-height-image">
                                    </div>
                                </div>`

                    }
                }
                $('.image-product').append(imageDiv);
                // $('#image-product-1').append(imageDiv);
                $(".product-images-array").val(JSON.stringify(images));
            }
            $(document).ready(function () {
                if (images.length > 0) {
                    setTimeout(() => {
                        apeendImaged();
                    }, 1000)
                }
                $('.product_upload_image_input').on('change', function () {
                    $("#overlay").fadeIn(300);
                    $(".product-upload-image").attr('disabled','disabled');
                    var fileData = $(this).prop("files")[0];
                    var formData = new FormData();
                    formData.append("image", fileData);
                    var url = url + 'upload-image';
                    if (url.length > 0) {
                        $.ajax({
                            url: "{{ url('/en') }}/admin/upload-image",
                            type: 'post',
                            dataType: 'json',
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: formData,
                            headers: {
                                'X-CSRF-TOKEN': '{{ csrf_token() }}'
                            }
                        })
                            .done(function (res) {

                                // $('.public_select_image').val(res.data);
                                // let img  = imageUrl(window.Laravel.base+res.data, 120, 120, 100, 1);

                                images.push([res.data]);
                                apeendImaged();
                                $("#overlay").fadeOut(300);
                                $('.product-upload-image').removeAttr('disabled');
                                toastr.success(res.message, 'Success');
                                
                            })
                            .fail(function (res) {
                                $("#overlay").fadeOut(300);
                                alert('Something went wrong, please try later.');
                            });
                    }
                    
                });
                $('.select_image').hide();
            })

    </script>
@endsection