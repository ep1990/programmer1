@extends('admin.layouts.app')

@section('css')

<style>
    .card {
        height: 400px;
        margin-top: 20px !important;
        position: relative;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: none;
        border-radius: 0.25rem;
        position: relative;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: none;
        border-radius: 0.25rem;
    }
</style>

@endsection

@section('content')

<main class="py-4">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        @if(session()->has('success'))
                            <p class="alert alert-success">{{ session('success') }}</p>
                        @endif
                        @if(session()->has('error'))
                            <p class="alert alert-danger">{{ session('error') }}</p>
                        @endif
                        <div class="card">
                            <div class="card-header">
                                <a href="{{ route('admin.product', 0) }}" class="btn btn-primary">Add Product</a>
                            </div>
                            <div class="card-body">
                               <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <td>#</td>
                                            <td>Title</td>
                                            <td>Description</td>
                                            <td>Quantity</td>
                                            <td>Price</td>
                                            <td colspan="2">Action</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($products as $product)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $product->title }}</td>
                                                <td>{{ $product->description }}</td>
                                                <td>{{ $product->quantity}}</td>
                                                <td>{{ $product->price}}</td>
                                                <td><a href="{{ route('admin.product', $product->id) }}" class="badge badge-success">Edit</a></td>
                                                <td><a href="{{ route('admin.delete-product', $product->id) }}" onclick="return confirm('Are you sure ?') " class="badge badge-warning">Delete</a></td>
                                            </tr>
                                        @empty

                                        @endforelse
                                    </tbody>
                               </table>
                               {{ $products->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>

@endsection